package net.sf.okapi.lib.xliff2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.xml.namespace.QName;

import net.sf.okapi.lib.xliff2.core.ExtElement;
import net.sf.okapi.lib.xliff2.core.Segment;
import net.sf.okapi.lib.xliff2.core.Unit;

import org.junit.Test;

public class UtilTest {

	@Test
	public void testIsValidNmtoken () {
		String supChar = new StringBuilder().appendCodePoint(0x20B9F).toString();
		// Valid
		assertEquals(true, Util.isValidNmtoken("123"));
		assertEquals(true, Util.isValidNmtoken("abc"));
		assertEquals(true, Util.isValidNmtoken("\u0100etc"));
		assertEquals(true, Util.isValidNmtoken(".id"));
		assertEquals(true, Util.isValidNmtoken("id"+supChar));
		// Invalid
		assertEquals(false, Util.isValidNmtoken("#id"));
		assertEquals(false, Util.isValidNmtoken("val/"));
		assertEquals(false, Util.isValidNmtoken("id&"));
		assertEquals(false, Util.isValidNmtoken("~ab"));
		assertEquals(false, Util.isValidNmtoken("a=b"));
		assertEquals(false, Util.isValidNmtoken("id$"));
		assertEquals(false, Util.isValidNmtoken(supChar+"@"));
		assertEquals(false, Util.isValidNmtoken("id|"));
	}
	
	@Test
	public void testRemoveExtensions () {
		Unit unit = new Unit("u1");
		Segment seg = unit.appendSegment();
		seg.setSource("text");
		unit.getExtAttributes().setAttribute("myNS", "attr1", "val1");
		unit.getExtAttributes().setAttribute(Const.NS_XLIFF_FS20, "fs", "p");
		unit.getExtAttributes().setAttribute("myNS", "attr3", "val3");
		unit.getExtAttributes().setAttribute("myNS", "attr4", "val4");
		unit.getExtElements().add(new ExtElement(new QName("myNS", "elem1")));
		unit.getExtElements().add(new ExtElement(new QName("myNS", "elem2")));
		unit.getExtElements().add(new ExtElement(new QName(Const.NS_XLIFF_GLOSSARY20, "gloss"))); // Invalid, but it's just for test
		unit.getExtElements().add(new ExtElement(new QName("myNS", "elem4")));
		
		assertEquals(true, unit.hasExtAttribute());
		assertEquals(4, unit.getExtAttributes().size());
		assertEquals(true, unit.hasExtElements());
		assertEquals(4, unit.getExtElements().size());
		
		Util.removeExtensions(unit);
		
		assertEquals(1, unit.getExtAttributes().size());
		assertEquals("p", unit.getExtAttributeValue(Const.NS_XLIFF_FS20, "fs"));
		assertEquals(1, unit.getExtElements().size());
		assertEquals("gloss", unit.getExtElements().get(0).getQName().getLocalPart());
	}

	@Test
	public void testLang () {
		// Valid
		assertTrue(Util.validateLang("en")==null);
		assertTrue(Util.validateLang("en-us")==null);
		assertTrue(Util.validateLang("i-klingon")==null);
		assertTrue(Util.validateLang("mN-cYrL-Mn")==null);
		assertTrue(Util.validateLang("en-x-US")==null);
		assertTrue(Util.validateLang("es-419")==null);
		assertTrue(Util.validateLang("az-Arab-x-AZE-derbend")==null);
		assertTrue(Util.validateLang("sl-Latn-IT-rozaj")==null);
		assertTrue(Util.validateLang("zh-cmn-Hant-HK")==null);
		assertTrue(Util.validateLang("en-Latn-GB-boont-r-extended-sequence-x-private")==null);
		
		// Not valid
		assertFalse(Util.validateLang(null)==null);
		assertFalse(Util.validateLang("")==null);
		assertFalse(Util.validateLang("f-Latn")==null);
		assertFalse(Util.validateLang("fra-FX")==null);
		assertFalse(Util.validateLang("zh-Latm-CN")==null);
		assertFalse(Util.validateLang("de-DE-1902")==null);
		assertFalse(Util.validateLang("fr-shadok")==null);
	}
	
	@Test
	public void testSupports () {
		// Supported directly
		assertTrue(Util.supports(Const.NS_XLIFF_MATCHES20));
		assertTrue(Util.supports(Const.NS_XLIFF_METADATA20));
		assertTrue(Util.supports(Const.NS_XLIFF_GLOSSARY20));
		assertTrue(Util.supports(Const.NS_XLIFF_VALIDATION20));
		// Not directly supported
		assertFalse(Util.supports(Const.NS_XLIFF_FS20));
		assertFalse(Util.supports(Const.NS_XLIFF_RESDATA20));
		assertFalse(Util.supports(Const.NS_XLIFF_SIZE20));
		assertFalse(Util.supports(Const.NS_XLIFF_TRACKING20));
	}

	@Test
	public void testIsValidXML () {
		assertTrue(Util.isValidInXML((int)'c'));
		assertTrue(Util.isValidInXML(0x10FFFF));
		assertFalse(Util.isValidInXML(0x7FFFFFFF));
		assertFalse(Util.isValidInXML(0x8FFFFFFF));
		assertFalse(Util.isValidInXML(0x110000));
		assertFalse(Util.isValidInXML((int)'\u000C'));
	}

}
