/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.its;

/**
 * Represents an ITS data category or a group of several instances of an ITS data category.
 */
public interface IITSItem {

	/**
	 * Gets the id/name of this data category. 
	 * @return the id/name of this data category.
	 */
	public String getDataCategoryName ();
	
	/**
	 * Indicates if this item is a group.
	 * @return true if it is a group, false if it is a standalone data category instance.
	 */
	public boolean isGroup ();
	
	/**
	 * Indicates if this item has currently a reference to a stand-off element that
	 * has not been resolved yet.
	 * <p>This occurs for example when a unit element has a reference to a set of Provenance instances
	 * and the stand-off element has not been read yet (because the reading of the unit's element
	 * is done after the reading of its attributes.
	 * @return true if this item has currently an unresolved reference to a stand-off element.
	 */
	public boolean hasUnresolvedGroup ();
	
	/**
	 * Sets the annotator reference information for this data category.
	 * @param annotatorRef the reference string to set (can be null).
	 */
	public void setAnnotatorRef (String annotatorRef);

	/**
	 * Sets the annotator reference information for this data category.
	 * @param ar the set of references read from <code>its:annotatorsRef</code>.
	 * If it is null, or if there is no reference for the relevant data category: no change is made. 
	 */
	public void setAnnotatorRef (AnnotatorsRef ar);
	
	/**
	 * Gets the annotator reference currently set for this data category.
	 * This method is not be supported for items that are data category groups.
	 * @return the annotator reference currently set for this data category.
	 */
	public String getAnnotatorRef ();

	/**
	 * Validates the data category.
	 * Checks if all required attributes are set properly.
	 * @throws XLIFFException if there is an error.
	 */
	public void validate ();

	/**
	 * Creates a deep-copy clone of this item.
	 * @return the duplicated item.
	 */
	public IITSItem createCopy ();

}
