/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.document;

import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.core.Unit;

/**
 * Represents a unit node.
 */
public class UnitNode extends GroupOrUnitNode {

	private Unit data;

	/**
	 * Creates a new {@link UnitNode} with a given {@link Unit} resource.
	 * @param data the unit to set for this unit node (must not be null).
	 */
	public UnitNode (Unit data) {
		if ( data == null ) {
			throw new InvalidParameterException("The data associated with the new unit node must not be null.");
		}
		this.data = data;
	}
	
	/**
	 * Gets the {@link Unit} associated with this unit node.
	 * @return the unit object associated with this unit node.
	 */
	public Unit get () {
		return data;
	}

}
