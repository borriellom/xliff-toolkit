/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.Const;
import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.Util;

/**
 * Represents common data for the opening and closing {@link CTag}.
 */
class CTagCommon {

	private static final int CANCOPY = 0x01;
	private static final int CANDELETE = 0x02;

	private String id;
	private String type;
	private int hints = (CANCOPY | CANDELETE);
	private boolean canOverlap;
	private String subType;
	private String copyOf;
	private Directionality dir = Directionality.INHERITED;
	
	CTagCommon (String id) {
		if ( id == null ) {
			throw new InvalidParameterException("The is parameter cannot be null.");
		}
		this.id = id;
	}

	/**
	 * Copy constructor.
	 * @param original the original object to copy.
	 */
	CTagCommon (CTagCommon original) {
		this.canOverlap = original.canOverlap;
		this.copyOf = original.copyOf;
		this.dir = original.dir;
		this.hints = original.hints;
		this.id = original.id;
		this.subType = original.subType;
		this.type = original.type;
	}
	
	public String getId () {
		return id;
	}

	public void setId (String id) {
		this.id = id;
	}

	public String getType () {
		return type;
	}

	public void setType (String type) {
		Util.checkValueList("fmt;ui;quote;link;image;other", type, Const.ATTR_TYPE);
		// Allows null value
		this.type = type;
	}

	public String getSubType () {
		return subType;
	}

	public void setSubType (String subType) {
		if ( subType != null ) {
			int n = subType.indexOf(':');
			if (( n == -1 ) || ( n == 0 ) || ( n == subType.length()-1 )) {
				throw new InvalidParameterException(String.format("Invalid value '%s' for subType.", subType));
			}
			if ( subType.startsWith("xlf:") ) {
				Util.checkValueList("xlf:lb;xlf:pb;xlf:b;xlf:i;xlf:u;xlf:var", subType, Const.ATTR_SUBTYPE);
			}
		}
		this.subType = subType;
	}

	public boolean getCanCopy () {
		return (( hints & CANCOPY ) == CANCOPY);
	}

	public void setCanCopy (boolean canCopy) {
		if ( canCopy ) hints |= CANCOPY;
		else hints &= ~CANCOPY;
	}

	public boolean getCanDelete () {
		return (( hints & CANDELETE ) == CANDELETE);
	}

	public void setCanDelete (boolean canDelete) {
		if ( canDelete ) hints |= CANDELETE;
		else hints &= ~CANDELETE;
	}

	public boolean getCanOverlap () {
		return canOverlap;
	}

	public void setCanOverlap (boolean canOverlap) {
		this.canOverlap = canOverlap;
	}

	public String getCopyOf () {
		return copyOf;
	}

	public void setCopyOf (String copyOf) {
		this.copyOf = copyOf;
	}

	public Directionality getDir () {
		return dir;
	}

	public void setDir (Directionality dir) {
		this.dir = dir;
	}

	public boolean equals (CTagCommon other) {
		if ( other == null ) return false;
		if ( this == other) return true;
		if ( Util.compareAllowingNull(getId(), other.getId()) != 0 ) return false;
		if ( Util.compareAllowingNull(getType(), other.getType()) != 0 ) return false;
		if ( Util.compareAllowingNull(getSubType(), other.getSubType()) != 0 ) return false;
		if ( Util.compareAllowingNull(getCopyOf(), other.getCopyOf()) != 0 ) return false;
		if ( getDir() != other.getDir() ) return false;
		if ( getCanCopy() != other.getCanCopy() ) return false;
		if ( getCanDelete() != other.getCanDelete() ) return false;
		if ( getCanOverlap() != other.getCanOverlap() ) return false;
		return true;
	}

}
