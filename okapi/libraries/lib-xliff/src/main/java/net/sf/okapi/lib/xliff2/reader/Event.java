/*===========================================================================
  Copyright (C) 2011-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.reader;

import net.sf.okapi.lib.xliff2.core.InsingnificantPartData;
import net.sf.okapi.lib.xliff2.core.MidFileData;
import net.sf.okapi.lib.xliff2.core.Skeleton;
import net.sf.okapi.lib.xliff2.core.StartFileData;
import net.sf.okapi.lib.xliff2.core.StartGroupData;
import net.sf.okapi.lib.xliff2.core.StartXliffData;
import net.sf.okapi.lib.xliff2.core.Unit;

/**
 * Represents an event send by the the {@link XLIFFReader}. 
 */
public class Event {

	private EventType type;
	private Object object;
	private URIContext uriCtx;
	
	public Event (EventType type,
		URIContext uriCtx)
	{
		this.type = type;
		this.uriCtx = uriCtx;
	}
	
	public Event (EventType type,
		URIContext uriCtx,
		Object object)
	{
		this.type = type;
		this.uriCtx = uriCtx;
		this.object = object;
	}
	
	public EventType getType () {
		return this.type;
	}
	
	public URIContext getURIContext () {
		return uriCtx;
	}

	public boolean isStartDocument () {
		return (type == EventType.START_DOCUMENT);
	}
	
	public boolean isEndDocument () {
		return (type == EventType.END_DOCUMENT);
	}

	public boolean isStartXliff () {
		return (type == EventType.START_XLIFF);
	}
	
	public StartXliffData getStartXliffData () {
		return (StartXliffData)object;
	}
	
	public boolean isEndXliff () {
		return (type == EventType.END_XLIFF);
	}
	
	public boolean isStartFile () {
		return (type == EventType.START_FILE);
	}
	
	public StartFileData getStartFileData () {
		return (StartFileData)object;
	}

	public boolean isMidFile () {
		return (type == EventType.MID_FILE);
	}
	
	public MidFileData getMidFileData () {
		return (MidFileData)object;
	}

	public boolean isEndFile () {
		return (type == EventType.END_FILE);
	}
	
	public boolean isSkeleton () {
		return (type == EventType.SKELETON);
	}
	
	public Skeleton getSkeletonData () {
		return (Skeleton)object;
	}
	
	public boolean isStartGroup () {
		return (type == EventType.START_GROUP);
	}
	
	public StartGroupData getStartGroupData () {
		return (StartGroupData)object;
	}
	
	public boolean isEndGroup () {
		return (type == EventType.END_GROUP);
	}
	
	public boolean isUnit () {
		return (type == EventType.TEXT_UNIT);
	}
	
	public Unit getUnit () {
		return (Unit)object;
	}

	public InsingnificantPartData getInsingnificantPartData () {
		return (InsingnificantPartData)object;
	}

	public Object getResource () {
		return object;
	}
}
