/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.its;

import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.XLIFFException;

/**
 * Implements the <a href='http://www.w3.org/TR/its20/#mtconfidence'>MT Confidence</a> data category.
 */
public class MTConfidence extends DataCategory {

	private Double mtConfidence;

	/**
	 * Creates a new {@link MTConfidence} object without initial data.
	 */
	public MTConfidence () {
		// Needed in some cases
	}

	/**
	 * Creates a new {@link MTConfidence} object with a given annotator reference and confidence score.
	 * @param annotatorRef the annotator reference.
	 * @param mtConfidence the confidence score.
	 */
	public MTConfidence (String annotatorRef,
		double mtConfidence)
	{
		setAnnotatorRef(annotatorRef);
		setMtConfidence(mtConfidence);
	}

	@Override
	public String getDataCategoryName () {
		return DataCategories.MTCONFIDENCE;
	}
	
	@Override
	public void validate () {
		if (( mtConfidence != null ) && ( getAnnotatorRef() == null )) {
			throw new XLIFFException("An annotator reference must be defined when mtConfidence is defined.");
		}
	}

	public Double getMtConfidence () {
		return mtConfidence;
	}
	
	public void setMtConfidence (Double mtConfidence) {
		if ( mtConfidence != null ) {
			if (( mtConfidence < 0.0 ) || ( mtConfidence > 1.0 )) {
				throw new InvalidParameterException(String.format("The mtConfidence value '%f' is out of the [0.0 to 1.0] range.", mtConfidence));
			}
		}
		this.mtConfidence = mtConfidence;
	}

	@Override
	public IITSItem createCopy () {
		return new MTConfidence(getAnnotatorRef(), mtConfidence);
	}

}
