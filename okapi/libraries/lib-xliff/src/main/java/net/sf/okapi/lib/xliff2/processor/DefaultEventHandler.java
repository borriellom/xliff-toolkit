/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.processor;

import net.sf.okapi.lib.xliff2.reader.Event;

/**
 * Implements a default event handler for {@link XLIFFProcessor}.
 * Each handler does nothing. You can derive your own event handler from this class
 * and define only the handlers you need.
 */
public class DefaultEventHandler implements IEventHandler {

	@Override
	public Event handleStartDocument (Event event) {
		return event;
	}

	@Override
	public Event handleEndDocument (Event event) {
		return event;
	}

	@Override
	public Event handleStartXliff (Event event) {
		return event;
	}

	@Override
	public Event handleEndXliff (Event event) {
		return event;
	}

	@Override
	public Event handleStartFile (Event event) {
		return event;
	}

	@Override
	public Event handleSkeleton (Event event) {
		return event;
	}

	@Override
	public Event handleMidFile (Event event) {
		return event;
	}

	@Override
	public Event handleEndFile (Event event) {
		return event;
	}

	@Override
	public Event handleStartGroup (Event event) {
		return event;
	}

	@Override
	public Event handleEndGroup (Event event) {
		return event;
	}

	@Override
	public Event handleInsignificantPart (Event event) {
		return event;
	}

	@Override
	public Event handleUnit (Event event) {
		return event;
	}
	
}
