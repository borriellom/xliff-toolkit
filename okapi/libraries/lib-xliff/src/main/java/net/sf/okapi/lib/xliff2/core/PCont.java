/*===========================================================================
Copyright (C) 2011-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
This library is free software; you can redistribute it and/or modify it 
under the terms of the GNU Lesser General Public License as published by 
the Free Software Foundation; either version 2.1 of the License, or (at 
your option) any later version.

This library is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License 
along with this library; if not, write to the Free Software Foundation, 
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Represents the standalone tag of a protected content.
 * <p>A protected content is a span of content (that may include code or marker tags) that is temporarily
 * folded into a tag reference to prevent the content to be modified.
 * <p>See {@link Unit#hideProtectedContent()} and {@link Unit#showProtectedContent()}.
 */
public class PCont {
	
	protected String codedText;

	/**
	 * Creates an empty {@link PCont} object.
	 */
	public PCont () {
		// Argument-less constructor
	}
	
	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public PCont (PCont original) {
		this(original.codedText);
	}
	
	/**
	 * Creates a new protected content marker with a given coded text.
	 * @param codedText the span of coded text to protect.
	 */
	public PCont (String codedText) {
		setCodedText(codedText);
	}

//	/**
//	 * Indicates if this marker is equal to another.
//	 * <p>Use the <code>=</code> operator to test if two markers are the same.
//	 * @param marker the other marker to compare to this one.
//	 * @return true if both markers are equals.
//	 */
//	//boolean equals (PMarker marker);

	/**
	 * Gets the coded text for this protected content.
	 * The corresponding markers are in the store.
	 * @return the coded text for this protected content.
	 */
	public String getCodedText () {
		return codedText;
	}

	/**
	 * Sets the coded text for this protected content.
	 * @param codedText the new coded text.
	 */
	public void setCodedText (String codedText) {
		this.codedText = codedText;
	}

}
