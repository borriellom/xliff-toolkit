/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.validation;

/**
 * Represents an issue detected when processing the rules for the Validation module.
 */
public class Issue {

	private String code;
	private String text;
	private String fileId;
	private String unitId;
	private String ruleInfo;
	
	/**
	 * Creates a new {@link Issue} object with a given segment number, code and text.
	 * @param fileId the id of the file for the given unit.
	 * @param code the string representation of the code for this issue.
	 * @param text the human readable text of this issue.
	 */
	public Issue (String fileId,
		String unitId,
		String code,
		String text,
		String ruleInfo)
	{
		this.fileId = fileId;
		this.unitId = unitId;
		this.code = code;
		this.text = text;
		this.ruleInfo = ruleInfo;
	}

	/**
	 * Gets the code for this issue.
	 * @return the code for this issue.
	 */
	public String getCode () {
		return code;
	}

	/**
	 * Sets the code for this issue.
	 * @param code the code for this issue.
	 */
	public void setCode (String code) {
		this.code = code;
	}
	
	/**
	 * Gets the text for this issue.
	 * @return the text for this issue.
	 */
	public String getText () {
		return text;
	}
	
	/**
	 * Sets the text for this issue.
	 * @param text the text for this issue.
	 */
	public void setText (String text) {
		this.text = text;
	}

	/**
	 * Gets the id of the unit where this issue is.
	 * @return the id of the unit where this issue is.
	 */
	public String getUnitId () {
		return unitId;
	}

	/**
	 * Sets the id of the unit where this issue is.
	 * @param unitId the id of the unit where this issue is.
	 */
	public void setUnitId (String unitId) {
		this.unitId = unitId;
	}

	/**
	 * Gets the file id for this issue.
	 * @return the file id for this issue.
	 */
	public String getFileId () {
		return fileId;
	}

	/**
	 * Sets the file id for this issue.
	 * @param fileId the file id for this issue.
	 */
	public void setFileId (String fileId) {
		this.fileId = fileId;
	}

	/**
	 * Gets the string representation of the rule that triggered the issue.
	 * @return the the string representation of the rule that triggered the issue.
	 */
	public String getRuleInfo () {
		return ruleInfo;
	}

	/**
	 * Sets the string representation of the rule that triggered the issue.
	 * @param ruleInfo the string representation of the rule that triggered the issue.
	 */
	public void setRuleInfo (String ruleInfo) {
		this.ruleInfo = ruleInfo;
	}
	
}
