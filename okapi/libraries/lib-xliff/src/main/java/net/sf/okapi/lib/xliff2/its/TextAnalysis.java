/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.its;

import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.XLIFFException;

/**
 * Implements the <a href='http://www.w3.org/TR/its20/#textanalysis'>Text Analysis</a> data category.
 */
public class TextAnalysis extends DataCategory {

	private Double taConfidence;
	private String taClassRef;
	private String taSource;
	private String taIdent;
	private String taIdentRef;

	/**
	 * Creates an empty {@link TextAnalysis} object.
	 */
	public TextAnalysis () {
		// nothing to do
	}
	
	@Override
	public String getDataCategoryName () {
		return DataCategories.TEXTANALYSIS;
	}
	
	@Override
	public void validate () {
		if ( taClassRef == null ) {
			if (( taSource == null ) && ( taIdentRef == null )) {
				throw new XLIFFException("If taClassRef is not defined you must have either taSource/taIndent or taIdentRef defined.");
			}
		}
		if ( taSource != null ) {
			if ( taIdent == null ) {
				throw new XLIFFException("If taSource is defined taIndent must also be defined.");
			}
		}
		else {
			if ( taIdent != null ) {
				throw new XLIFFException("If taIdent is defined taSource must also be defined.");
			}
		}
		if ( taIdentRef != null ) {
			if (( taSource != null ) || ( taIdent != null )) {
				throw new XLIFFException("If taIdentRef is defined neither taSource nor taIdent can be defined.");
			}
		}
		if (( taConfidence != null ) && ( getAnnotatorRef() == null )) {
			throw new XLIFFException("An annotator reference must be defined when taConfidence is defined.");
		}
	}

	/**
	 * Gets the <code>taConfidence</code> attribute.
	 * @return the <code>taConfidence</code> attribute (can be null).
	 */
	public Double getTaConfidence () {
		return taConfidence;
	}
	
	/**
	 * Sets a new <code>taConfidence</code> attribute.
	 * @param taConfidence the new <code>taConfidence</code> attribute (between 0.0 and 1.0, or null).
	 */
	public void setTaConfidence (Double taConfidence) {
		if ( taConfidence != null ) {
			if (( taConfidence < 0.0 ) || ( taConfidence > 1.0 )) {
				throw new InvalidParameterException(String.format("The taConfidence value '%f' is out of the [0.0 to 1.0] range.", taConfidence));
			}
		}
		this.taConfidence = taConfidence;
	}

	public String getTaClassRef () {
		return taClassRef;
	}

	public void setTaClassRef (String taClassRef) {
		this.taClassRef = taClassRef;
	}

	/**
	 * Gets the <code>taSource</code> attribute.
	 * @return the <code>taSource</code> attribute (can be null).
	 */
	public String getTaSource () {
		return taSource;
	}

	/**
	 * Sets a new <code>taSource</code> attribute.
	 * @param taSource the new <code>taSource</code> attribute (can be null).
	 */
	public void setTaSource (String taSource) {
		this.taSource = taSource;
	}

	public String getTaIdent () {
		return taIdent;
	}

	public void setTaIdent (String taIdent) {
		this.taIdent = taIdent;
	}

	public String getTaIdentRef () {
		return taIdentRef;
	}

	public void setTaIdentRef (String taIdentRef) {
		this.taIdentRef = taIdentRef;
	}

	@Override
	public IITSItem createCopy () {
		TextAnalysis newItem = new TextAnalysis();
		newItem.setAnnotatorRef(getAnnotatorRef());
		newItem.taClassRef = taClassRef;
		newItem.taConfidence = taConfidence;
		newItem.taIdent = taIdent;
		newItem.taIdentRef = taIdentRef;
		newItem.taSource = taSource;
		return newItem;
	}

}
