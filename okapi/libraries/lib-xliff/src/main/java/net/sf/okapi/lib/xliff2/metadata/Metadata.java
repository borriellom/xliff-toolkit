/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.metadata;

import net.sf.okapi.lib.xliff2.core.BaseList;

/**
 * Represents a list of {@link IMetadataItem} objects.
 */
public class Metadata extends BaseList<MetaGroup> implements IWithMetaGroup {

	private String id;
	
	/**
	 * Creates an empty {@link Metadata} object.
	 */
	public Metadata () {
		// Nothing to do
	}
	
	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public Metadata (Metadata original) {
		super(original);
		this.id = original.id;
	}

	public String getId () {
		return id;
	}

	public void setId (String id) {
		this.id = id;
	}

	@Override
	public void addGroup (MetaGroup group) {
		add(group);
	}
	
}
