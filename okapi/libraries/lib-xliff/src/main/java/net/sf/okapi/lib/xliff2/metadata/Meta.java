/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.metadata;

import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.Util;

/**
 * Represents a meta element.
 */
public class Meta implements IMetadataItem {

	private String type;
	private String data;
	
	/**
	 * Creates a {@link Meta} object with a given type.
	 * @param type the type of the object (cannot be null or empty).
	 */
	public Meta (String type) {
		setType(type);
	}

	/**
	 * Creates a {@link Meta} object with a given type and data.
	 * @param type the type of the object (cannot be null or empty)
	 * @param data the data of the object (can be null).
	 */
	public Meta (String type,
		String data)
	{
		setType(type);
		setData(data);
	}
	
	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public Meta (Meta original) {
		type = original.type;
		data = original.data;
	}

	@Override
	public boolean isGroup () {
		return false;
	}
	
	public String getType () {
		return type;
	}

	public void setType (String type) {
		if ( Util.isNoE(type) ) {
			throw new InvalidParameterException("The type of a <meta> must not be null or empty.");
		}
		this.type = type;
	}

	public String getData () {
		return data;
	}

	public void setData (String data) {
		this.data = data;
	}

}
