/*===========================================================================
  Copyright (C) 2012-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.reader;

/**
 * List of the different types of {@link Event} objects the {@link XLIFFReader} can generate.
 */
public enum EventType {
	/**
	 * Start of the input document.
	 * This event has no associated resource.
	 * There are no events before this one.
	 */
	START_DOCUMENT,
	
	/**
	 * End of the input document.
	 * This event has no associated resource.
	 * There are no events after this one.
	 */
	END_DOCUMENT,
	
	/**
	 * Start of the XLIFF element of the document.
	 * This event comes with an {@link net.sf.okapi.lib.xliff2.core.StartXliffData StartXliffData} resource.
	 */
	START_XLIFF,
		
	/**
	 * End of the XLIFF element in the document.
	 * This event has no associated resource.
	 */
	END_XLIFF,
		
	/**
	 * Start of a file in an XLIFF document.
	 * This event comes with a {@link net.sf.okapi.lib.xliff2.core.StartFileData StartFileData} resource.
	 */
	START_FILE,
	
	/**
	 * Part of the file-level data after the skeleton and before the first unit or group.
	 * This event comes with a {@link net.sf.okapi.lib.xliff2.core.MidFileData MidFileData} resource.
	 */
	MID_FILE,
	
	/**
	 * End of a file in an XLIFF document.
	 * This event has no associated resource.
	 */
	END_FILE,
		
	/**
	 * Skeleton element.
	 * This event comes with a {@link net.sf.okapi.lib.xliff2.core.StartXliffData StartXliffData} resource.
	 */
	SKELETON,
		
	/**
	 * Start of a group.
	 * This event comes with a {@link net.sf.okapi.lib.xliff2.core.StartGroupData StartGroupData} resource.
	 */
	START_GROUP,
		
	/**
	 * End of a group.
	 * This event has no associated resource.
	 */
	END_GROUP,
		
	/**
	 * A full unit element (start to end)
	 * This event comes with a {@link net.sf.okapi.lib.xliff2.core.Unit Unit} resource.
	 */
	TEXT_UNIT,
		
	/**
	 * Non-significant parts of the document (white-spaces between elements
	 * outside the content).
	 * this event comes with a {@link net.sf.okapi.lib.xliff2.core.InsingnificantPartData InsingnificantPartData} resource.
	 */
	INSIGNIFICANT_PART
}
