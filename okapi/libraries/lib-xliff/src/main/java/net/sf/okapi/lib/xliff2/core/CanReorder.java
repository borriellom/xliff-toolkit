/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Represents the value of the canReorder field.
 * One of: {@link #YES}, {@link #FIRSTNO} or {@link #NO}.
 */
public enum CanReorder {

	YES("yes"),
	
	FIRSTNO("firstNo"),
	
	NO("no");

	private String name;

	/**
	 * Creates a new {@link CanReorder} object with a given name.
	 * @param name the name of the item to create.
	 */
	private CanReorder (String name) {
		this.name = name;
	}

	@Override
	public String toString () {
		return name;
	}
	
}
