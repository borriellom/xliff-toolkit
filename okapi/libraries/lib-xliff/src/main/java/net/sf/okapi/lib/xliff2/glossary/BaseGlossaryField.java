/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.glossary;

import net.sf.okapi.lib.xliff2.core.DataWithExtAttributes;

/**
 * Represents the base class for the fields of the {@link GlossEntry} object.
 */
abstract class BaseGlossaryField extends DataWithExtAttributes {

	private String text;
	private String source;
	
	/**
	 * Creates an empty {@link BaseGlossaryField} object.
	 */
	public BaseGlossaryField () {
		// Nothing to do
	}
	
	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public BaseGlossaryField (BaseGlossaryField original) {
		// Create the new object from its base class copy constructor
		super(original);
		this.text = original.text;
		this.source = original.source;
	}

	/**
	 * Get the text of this field.
	 * @return the text of this field (can be null).
	 */
	public String getText () {
		return text;
	}
	
	/**
	 * Sets the text of this field.
	 * @param text the new text of this field (can be null).
	 */
	public void setText (String text) {
		this.text = text;
	}
	
	/**
	 * Gets the source of this field.
	 * @return the source of this field (can be null).
	 */
	public String getSource () {
		return source;
	}
	
	/**
	 * sets the source of this field.
	 * @param source the new source of this field (can be null).
	 */
	public void setSource (String source) {
		this.source = source;
	}
	
}
