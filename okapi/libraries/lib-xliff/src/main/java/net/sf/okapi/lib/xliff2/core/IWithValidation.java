/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.validation.Validation;

/**
 * Provides the methods to add and retrieve validation rules for an object. 
 */
public interface IWithValidation {

	/**
	 * Indicates if the object has validation data.
	 */
	public boolean hasValidation ();
	
	/**
	 * Gets the {@link Validation} object for this unit, creates an empty of if there is none.
	 * @return the {@link Validation} object for this unit (can be empty, but never null).
	 */
	public Validation getValidation ();

	/**
	 * sets the {@link Validation} object for this unit.
	 * @param validation the new {@link Validation} object for this unit.
	 */
	public void setValidation (Validation validation);

}
