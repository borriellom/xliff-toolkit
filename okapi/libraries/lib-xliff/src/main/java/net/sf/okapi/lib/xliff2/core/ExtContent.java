/*===========================================================================
  Copyright (C) 2012-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Represents an extension content object: a {@link IExtChild} object of 
 * type {@link ExtChildType#TEXT} or {@link ExtChildType#CDATA}. 
 */
public class ExtContent implements IExtChild {

	private boolean cdata;
	private String text;

	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public ExtContent (ExtContent original) {
		this(original.text, original.cdata);
	}

	/**
	 * Creates a new {@link ExtContent} object.
	 * @param text the text of the content.
	 */
	public ExtContent (String text) {
		setText(text);
	}
	
	/**
	 * Creates a new {@link ExtContent} object in CDATA mode or not.
	 * @param text the text of the content.
	 * @param cdata true if the content should be written in a CDATA section, false otherwise.
	 */
	public ExtContent (String text,
		boolean cdata)
	{
		setText(text);
		setCData(cdata);
	}
	
	@Override
	public ExtChildType getType () {
		if ( cdata ) return ExtChildType.CDATA;
		else return ExtChildType.TEXT;
	}

	/**
	 * Indicates if the content is to be represented as CDATA.
	 * @return true if the content is to be represented as CDATA, false otherwise.
	 */
	public boolean getCData () {
		return cdata;
	}

	/**
	 * Sets the flag indicating if the content is to be represented as CDATA.
	 * @param cdata true to represent the content as CDATA.
	 */
	public void setCData (boolean cdata) {
		this.cdata = cdata;
	}

	/**
	 * Gets the content.
	 * @return the content.
	 */
	public String getText () {
		return text;
	}

	/**
	 * Sets the content.
	 * @param text the new content.
	 */
	public void setText (String text) {
		this.text = text;
	}
	
}
