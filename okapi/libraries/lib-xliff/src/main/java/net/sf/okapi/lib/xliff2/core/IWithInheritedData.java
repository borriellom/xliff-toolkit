/*===========================================================================
  Copyright (C) 2013-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.core.Directionality;
import net.sf.okapi.lib.xliff2.its.AnnotatorsRef;

/**
 * Represents the data inherited throughout a file element: translate, canResegment, srcDir and trgDir.
 */
public interface IWithInheritedData {

	public void setInheritableData (IWithInheritedData obj);

	/**
	 * Indicates if this object can be re-segmented by default.
	 * @return true if this object can be re-segmented by default, false otherwise.
	 */
	public boolean getCanResegment ();
	
	/**
	 * Sets the flag indicating if this object can be re-segmented by default.
	 * @param canResegment true if this object can be re-segmented by default, false otherwise.
	 */
	public void setCanResegment (boolean canResegment);
	
	/**
	 * Indicates if this object has translatable content by default.
	 * @return true if this object has translatable content by default, false otherwise.
	 */
	public boolean getTranslate ();
	
	/**
	 * Sets the flag indicating if this object has translatable content by default.
	 * @param translate true if this object has translatable content by default, false otherwise.
	 */
	public void setTranslate (boolean translate);
	
	public Directionality getSourceDir ();

	public void setSourceDir (Directionality dir);

	public Directionality getTargetDir ();

	public void setTargetDir (Directionality dir);

	/**
	 * Gets the annotators-references for this object.
	 * @return the annotators-references for this object (can be null)
	 */
	public AnnotatorsRef getAnnotatorsRef ();
	
	/**
	 * Sets the annotators-references for this object.
	 * @param annotators the new annotators-references for this object (can be null).
	 */
	public void setAnnotatorsRef (AnnotatorsRef annotators);

}
