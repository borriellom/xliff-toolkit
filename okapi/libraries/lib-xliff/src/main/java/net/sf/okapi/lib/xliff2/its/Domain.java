/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.its;

/**
 * Implements the <a href='http://www.w3.org/TR/its20/#domain'>Domain</a> data category.
 */
public class Domain extends DataCategory {

	private String domain;

	/**
	 * Creates a new {@link Domain} object without initial data.
	 */
	public Domain () {
		// Needed in some cases
	}

	/**
	 * Creates a new {@link Domain} object with a value.
	 * @param domain the value to set.
	 */
	public Domain (String domain) {
		setDomain(domain);
	}

	@Override
	public String getDataCategoryName () {
		return "domain";
	}
	
	@Override
	public void validate () {
		// Nothing to validate
	}

	@Override
	public IITSItem createCopy () {
		Domain newItem = new Domain(domain);
		newItem.setAnnotatorRef(getAnnotatorRef());
		
		return newItem;
	}

	public String getDomain () {
		return domain;
	}

	public void setDomain (String domain) {
		this.domain = domain;
	}

}
