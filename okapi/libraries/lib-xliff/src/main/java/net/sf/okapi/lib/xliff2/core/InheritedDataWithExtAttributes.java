/*===========================================================================
  Copyright (C) 2011-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;


/**
 * Implements an object implements the {@link IWithInheritedData}, {@link IWithExtAttributes}
 * and {@link IWithMetadata} interfaces. It also provides methods for id access.
 */
class InheritedDataWithExtAttributes extends InheritedData implements IWithExtAttributes {

	private ExtAttributes xattrs;
	private String id;
	
	/**
	 * Creates an empty {@link InheritedDataWithExtAttributes} object.
	 */
	protected InheritedDataWithExtAttributes () {
		// Nothing to do
	}
	
	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	protected InheritedDataWithExtAttributes (InheritedDataWithExtAttributes original) {
		// Create the new object from the base class copy constructor
		super(original);
		// Copy the InheritedDataWithExtAttributes-specific fields
		id = original.id;
		if ( original.hasExtAttribute() ) {
			xattrs = new ExtAttributes(original.xattrs);
		}
	}

	/**
	 * Sets the id for this object.
	 * @param id the id for this object.
	 */
	public void setId (String id) {
		this.id = id;
	}
	
	/**
	 * Gets the id for this object.
	 * @return the id for this object.
	 */
	public String getId () {
		return id;
	}

	@Override
	public void setExtAttributes (ExtAttributes attributes) {
		this.xattrs = attributes;
	}

	@Override
	public ExtAttributes getExtAttributes () {
		if ( xattrs == null ) {
			xattrs = new ExtAttributes();
		}
		return xattrs;
	}

	@Override
	public boolean hasExtAttribute () {
		if ( xattrs == null ) return false;
		return !xattrs.isEmpty();
	}

	@Override
	public String getExtAttributeValue (String namespaceURI,
		String localName)
	{
		if ( xattrs == null ) return null;
		return xattrs.getAttributeValue(namespaceURI, localName);
	}
	
}
