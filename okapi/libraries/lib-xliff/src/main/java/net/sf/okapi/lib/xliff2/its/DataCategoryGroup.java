/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.its;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Represents a stand-off group for a given data category that can have multiple
 * instances.
 */
public abstract class DataCategoryGroup<T extends DataCategory> implements IITSItem {

	private List<T> list;
	private String id;

//	@SuppressWarnings("unchecked")
//	public DataCategoryGroup (DataCategoryGroup<T> original) {
//		id = original.id;
//		list = new ArrayList<>();
//		for ( T item : original.list ) {
//			list.add((T)item.createClone());
//		}
//	}
	
	/**
	 * Creates a new {@link DataCategoryGroup} object with a given identifier.
	 * @param id the identifier to use (use null to create one automatically)
	 */
	public DataCategoryGroup (String id) {
		list = new ArrayList<>();
		if ( id == null ) this.id = UUID.randomUUID().toString();
		else this.id = id;
	}
	
	@Override
	public boolean isGroup () {
		return true;
	}

	@Override
	public boolean hasUnresolvedGroup () {
		return false; // Unresolved group do not occur in groups
	}

	/**
	 * Gets the identifier for this group.
	 * @return the identifier for this group.
	 */
	public String getGroupId () {
		return id;
	}

	/**
	 * Sets the identifier for this group. 
	 * @param id the identifier to set.
	 */
	public void setGroupId (String id) {
		this.id = id;
	}

	/**
	 * Gets the list of instances for this group.
	 * @return the list of instances for this group.
	 */
	public List<T> getList () {
		return list;
	}

	@Override
	public void setAnnotatorRef (String annotatorRef) {
		for ( T item : list ) {
			item.setAnnotatorRef(annotatorRef);
		}
	}

	@Override
	public void setAnnotatorRef (AnnotatorsRef ar) {
		for ( T item : list ) {
			item.setAnnotatorRef(ar);
		}
	}

	@Override
	public String getAnnotatorRef () {
		throw new UnsupportedOperationException("Data category groups do not support the getAnnotatorRef() method.");
	}
	
	@Override
	public void validate () {
		for ( T item : list ) {
			item.validate();
		}
	}

}
