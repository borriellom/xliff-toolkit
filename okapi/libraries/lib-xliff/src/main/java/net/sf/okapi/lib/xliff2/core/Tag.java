/*===========================================================================
Copyright (C) 2011-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
This library is free software; you can redistribute it and/or modify it 
under the terms of the GNU Lesser General Public License as published by 
the Free Software Foundation; either version 2.1 of the License, or (at 
your option) any later version.

This library is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License 
along with this library; if not, write to the Free Software Foundation, 
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Provides the common methods for code and marker tags.
 * <p>Any object deriving from this class must also provide an implementation 
 * for {@link CloneFactory#create(Tag, Tags)}. 
 * @see CTag
 * @see MTag
 */
abstract public class Tag extends DataWithExtAttributes {
	
	protected TagType tagType;

	/**
	 * Creates an empty {@link Tag} object.
	 */
	protected Tag () {
		// Nothing to do
	}
	
	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public Tag (Tag original) {
		// Create the new object from its base class copy constructor
		super(original);
		// Copy the specific fields
		tagType = original.tagType;
	}
	
	@Override
	public String toString () {
		return tagType+getId();
	}
	
	/**
	 * Gets the {@link TagType} value of this tag.
	 * @return the {@link TagType} value of this tag.
	 */
	public TagType getTagType () {
		return tagType;
	}

//	/**
//	 * Sets the {@link TagType} value of this tag.
//	 * @param tagType the {@link TagType} to set.
//	 */
//	public void setTagType (TagType tagType) {
//		this.tagType = tagType;
//	}
	
	/**
	 * Gets the id for the code or annotation using this tag.
	 * @return the id for the code or annotation using this tag.
	 */
	abstract public String getId ();
	
	/**
	 * Gets the type of the code or marker this tag represents.
	 * @return the type of the code or marker this tag represents.
	 * If this is a {@link CTag} the type value can be null.
	 */
	abstract public String getType ();
	
	/**
	 * Sets the type of the code or marker (for both opening/closing tags).
	 * @param type the type of the code or marker this tag represents.
	 */
	abstract public void setType (String type);

	/**
	 * Indicates if this tag is equal to another.
	 * <p>Use the <code>==</code> operator to test if two tags are the same.
	 * @param tag the other tag to compare to this one.
	 * @return true if both tags are equals.
	 */
	abstract boolean equals (Tag tag);

	/**
	 * Indicates if this tag is for a marker ({@link MTag}).
	 * @return true if this tag is one for a marker ({@link MTag}), false if it is for a code ({@link CTag}).
	 */
	abstract public boolean isMarker ();

	/**
	 * Indicates if this tag is for a code ({@link CTag}).
	 * @return true if this tag is used by a code ({@link CTag}), false if it is for a marker ({@link MTag}).
	 */
	abstract public boolean isCode ();

}
