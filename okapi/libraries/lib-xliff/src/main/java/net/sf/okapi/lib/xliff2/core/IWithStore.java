/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Represents an object that is the parent of a {@link Store} object, for example a {@link Unit} 
 * holds the store for the inline codes and other related data in that unit. 
 */
public interface IWithStore {

	/**
	 * Indicates if a given id value is already in use in the object (for a {@link Part} or for a {@link Tag}). 
	 * @param id the id value to lookup.
	 * @return true if the value is already used, false otherwise.
	 */
	public boolean isIdUsed (String id);

	public Directionality getSourceDir ();

	public void setSourceDir (Directionality dir);

	public Directionality getTargetDir ();

	public void setTargetDir (Directionality dir);

}
