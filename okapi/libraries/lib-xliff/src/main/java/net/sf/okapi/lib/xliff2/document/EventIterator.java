/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.document;

import java.util.Iterator;
import java.util.Stack;

import net.sf.okapi.lib.xliff2.reader.Event;
import net.sf.okapi.lib.xliff2.reader.URIContext;

/**
 * Represents the base class for events iterator for documents, files, groups and units.
 */
class EventIterator implements Iterator<Event> {

	protected Stack<URIContext> uriContext;

	/**
	 * Sets the URI context for this iterator.
	 * @param uriContext the context to set.
	 */
	public void setURIContext (Stack<URIContext> uriContext) {
		this.uriContext = uriContext;
	}

	@Override
	public boolean hasNext () {
		return false;
	}

	@Override
	public Event next () {
		return null;
	}

	@Override
	public void remove () {
		throw new UnsupportedOperationException("Remove is not supported.");
	}

}
