/*===========================================================================
  Copyright (C) 2013-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Represents the information associated with a file element.
 */
public class StartFileData extends InheritedDataWithExtAttributes {
	
	private String original;

	/**
	 * Creates a {@link StartFileData} object with an optional id.
	 * @param id the id of the file element (should not be null, but we don't throw an 
	 * exception to allow setting it after creation).
	 */
	public StartFileData (String id) {
		setId(id);
	}
	
	/**
	 * Gets the original attribute of this file.
	 * @return the original attribute of this file.
	 */
	public String getOriginal () {
		return original;
	}
	
	/**
	 * Sets the original attribute of this file.
	 * @param original the new original attribute for this file.
	 */
	public void setOriginal (String original) {
		this.original = original;
	}

}
