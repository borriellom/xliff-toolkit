/*===========================================================================
  Copyright (C) 2011-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import javax.xml.namespace.QName;

import net.sf.okapi.lib.xliff2.Const;

/**
 * Represents an extension (or unsupported module) attribute.
 */
public class ExtAttribute {

	private final QName qName;
	private String value;

	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public ExtAttribute (ExtAttribute original) {
		this(original.qName, original.value);
	}
	
	/**
	 * Creates a new {@link ExtAttribute} object.
	 * @param qName the qualified name of this attribute.
	 * @param value the value of this attribute.
	 */
	public ExtAttribute (QName qName,
		String value)
	{
		this.qName = qName;
		this.value = value;
	}

	/**
	 * Gets the QName of this attribute.
	 * @return the QName of this attribute.
	 */
	public QName getQName () {
		return qName;
	}
	
	/**
	 * Gets the value of this attribute.
	 * @return the value of this attribute (can be null).
	 */
	public String getValue () {
		return value;
	}
	
	/**
	 * Sets the value of this attribute.
	 * @param value the new value of this attribute (can be null).
	 */
	public void setValue (String value) {
		this.value = value;
	}
	
	/**
	 * Gets the local name of this attribute.
	 * @return the local name of this attribute.
	 */
	public String getLocalPart () {
		return qName.getLocalPart();
	}
	
	/**
	 * Gets the namespace URI of this attribute.
	 * @return the namespace URI of this attribute.
	 */
	public String getNamespaceURI () {
		return qName.getNamespaceURI();
	}
	
	/**
	 * Gets the prefix of this attribute.
	 * @return the prefix of this attribute.
	 */
	public String getPrefix () {
		return qName.getPrefix();
	}

	/**
	 * Indicates if this attribute is part of a module or not.
	 * @return true if this attribute is part of a module.
	 */
	public boolean isModule () {
		return qName.getNamespaceURI().startsWith(Const.NS_XLIFF_MODSTART);
	}
	
}
